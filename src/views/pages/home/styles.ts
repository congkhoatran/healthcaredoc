import styled from 'styled-components';

export const Container = styled.div`
  margin: 0;
  position: absolute;
  top: 30%;
  left: 50%;
  width: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

export const FieldSetSearch = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SearchContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const SearchLabel= styled.label`
  margin-bottom: 15px;
`;

export const UserLabel= styled.label`
  
`;

export const FieldSetUser = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  height: 400px
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  height: 550px;
  justify-content: space-evenly;

  .search {
    flex: 0.8;
  }

  p {
    color: #bf1650;
    font-size: 12px;
  }
  
  p::before {
    display: inline;
    content: "⚠ ";
  }
`;

export const TextField = styled.input`
  height: 25px;
  border: 1px solid #4c9196;
  border-radius: 10px;
  outline: none;
  padding: 10px;
`;

export const SubmitButton = styled.input`
  border-radius: 10px;
  background-color: #4db2b9;
  color: white;
  height: 46px;
  width: 120px;
  outline: none;
  border: none;
  font-weight: 600;
`;

export const BirthAndSexContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  input {
    flex: 0.45;
  }

  select {
    flex: 0.48;
    outline: none;
    border: 1px solid #4c9196;
    border-radius: 10px;
  }
`;

export const PhoneContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  input {
    flex: 0.45;
  }
`;


