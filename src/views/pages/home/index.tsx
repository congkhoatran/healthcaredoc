import React, { useEffect, useState } from 'react';
import * as Styled from './styles';
import { useForm } from "react-hook-form";
import useAxios from 'axios-hooks'

const HomePage: React.FC = () => {
  const { register, handleSubmit, watch, setValue, formState: { errors } } = useForm();
  const [{ data, loading, error }, searchUser] = useAxios(
    {
      url: `https://dev.api-rest.speedoc.com/users?search=${watch('searchData')}`,
      method: 'GET',
      headers: {
        'x-api-key': localStorage.getItem('secretKey'),
        'Authorization': localStorage.getItem('jwt')
      }
    },
    { manual: true }
  );

  const onSubmit = (data: any) => {
    searchUser();
  };

  useEffect(() => {
    if (data) {
      setValue('fullName', data.users[0].name);
      setValue('emailAddress', data.users[0].email.address);
      setValue('birthday', data.users[0].birthday);
      setValue('gender', data.users[0].gender);
      setValue('area', data.users[0].area);
      setValue('phoneNumber', data.users[0].phoneNumbers[0]);
    }
  }, [data, setValue])


  return (
    <Styled.Container>
      <Styled.Form onSubmit={handleSubmit(onSubmit)}>
        <Styled.FieldSetSearch>
          <Styled.SearchLabel>SEARCH EXISTING USER</Styled.SearchLabel>
          <Styled.SearchContainer>
            <Styled.TextField placeholder='Email or Phone or FullName' className={'search'} {...register("searchData", { required: true })} />
            <Styled.SubmitButton type="submit" value='Search' />
          </Styled.SearchContainer>
          {errors?.searchData?.type === "required" && <p>This field is required</p>}
          {loading && 'Loading........'}
        </Styled.FieldSetSearch>
        <Styled.FieldSetUser>
          <Styled.UserLabel>USER</Styled.UserLabel>
          <Styled.TextField placeholder='Full Name' {...register("fullName")} />
          <Styled.TextField placeholder='Email Address' {...register("emailAddress")}/>
          <Styled.BirthAndSexContainer>
            <Styled.TextField placeholder='Birthday'  {...register("birthday")}/>
            <select {...register("gender")}>
              <option value="M">Male</option>
              <option value="F">Female</option>
            </select>
          </Styled.BirthAndSexContainer>
          <Styled.PhoneContainer>
            <Styled.TextField placeholder='+65'  {...register("area")}/>
            <Styled.TextField placeholder='Phone number'  {...register("phoneNumber")}/>
          </Styled.PhoneContainer>
        </Styled.FieldSetUser>
      </Styled.Form>
    </Styled.Container>
  );
};

export default HomePage as React.ComponentType;
