import styled from 'styled-components';
import Logo from '../../../images/logo.png';


export const Container = styled.div`
  margin: 0;
  position: absolute;
  top: 30%;
  left: 50%;
  width: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

export const Banner = styled.div`
  height: 80px;
  background-image: url(${Logo});
  background-size: contain;
  float: right;
  margin: 5px 10px 0 0;
  background-repeat: no-repeat;
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  height: 550px;
  justify-content: space-evenly;

  p {
    color: #bf1650;
    font-size: 12px;
  }
  
  p::before {
    display: inline;
    content: "⚠ ";
  }
`;

export const TextFieldContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 75px;
  justify-content: space-between;
`;

export const TextField = styled.input`
  height: 25px;
  border: 1px solid #4c9196;
  border-radius: 10px;
  outline: none;
  padding: 10px;
`;

export const TextFieldLabel = styled.label`
`;

export const RememberContainer = styled.div`
`;

export const RememberCheckBox = styled.input`
`;
export const RememberLabel = styled.label`
  margin-left: 10px;
`;

export const SubmitButton = styled.input`
  border-radius: 10px;
  background-color: #4db2b9;
  color: white;
  height: 36px;
  outline: none;
  border: none;
  font-weight: 600;
`;
