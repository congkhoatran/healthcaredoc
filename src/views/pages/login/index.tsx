import React, { useEffect, useState } from 'react';
import * as Styled from './styles';
import { useForm } from "react-hook-form";
import useAxios from 'axios-hooks'
import { useHistory } from 'react-router-dom';
import routers_string from '../../utilities/routers';

const LoginPage: React.FC = () => {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [{ data, loading, error }, signIn] = useAxios(
    {
      url: 'https://dev.api-rest.speedoc.com/session/login',
      method: 'POST',
      headers: {
        'x-api-key': watch("secretKey")
      }
    },
    { manual: true }
  );

  const history = useHistory();

  const onSubmit = async (data: any) => {
    const requestLogin = {
      password: data.password,
      email: data.email,
      rememberMe: data.remember
    };
    localStorage.setItem('secretKey', data.secretKey);
    await signIn({
      data: {
        ...requestLogin
      }
    });
  } 
  
  useEffect(() => {
    if (data && data.jwt) {
      localStorage.setItem('jwt', data.jwt);
      history.push(routers_string.HOME);
    }
  }, [data, history])

  return (
    <Styled.Container>
      <Styled.Form onSubmit={handleSubmit(onSubmit)}>
        <Styled.Banner />
        <Styled.TextFieldContainer>
          <Styled.TextFieldLabel>Enter your email address</Styled.TextFieldLabel>
          <Styled.TextField placeholder='Your email address' {...register("email", { required: true, pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i })} />
          {errors?.email?.type === "required" && <p>This field is required</p>}
          {errors?.email?.type === "pattern" && (
            <p>Email format incorrect</p>
          )}
        </Styled.TextFieldContainer>
        <Styled.TextFieldContainer>
          <Styled.TextFieldLabel>Enter your secret Speedoc key</Styled.TextFieldLabel>
          <Styled.TextField placeholder='Your secret key'  {...register("secretKey", { required: true })} />
          {errors?.secretKey?.type === "required" && <p>This field is required</p>}
        </Styled.TextFieldContainer>
        <Styled.TextFieldContainer>
          <Styled.TextFieldLabel>Enter your password</Styled.TextFieldLabel>
          <Styled.TextField type='password' {...register("password", { required: true })} />
          {errors?.password?.type === "required" && <p>This field is required</p>}
        </Styled.TextFieldContainer>
        <Styled.RememberContainer>
          <Styled.RememberCheckBox type="checkbox" {...register("remember")} />
          <Styled.RememberLabel>Remember</Styled.RememberLabel>
        </Styled.RememberContainer>
        <Styled.SubmitButton type="submit" value='Login' />
        {loading && 'Loading........'}
      </Styled.Form>
    </Styled.Container>
  );
};

export default LoginPage as React.ComponentType;
